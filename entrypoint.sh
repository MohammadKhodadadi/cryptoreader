#!/bin/bash
set -e

python manage.py migrate  &

celery -A position worker -E --logfile=/app/logs/celery.log --loglevel=INFO &
celery -A position beat -l info --scheduler django_celery_beat.schedulers:DatabaseScheduler

# Start the default command provided
exec "$@"

